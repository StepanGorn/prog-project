import datetime as dt

from aiogram import types
from aiogram.dispatcher import FSMContext

from ..bot import bot, dispatcher
from ..utils import markups
from ..utils.database_models import Reminder
from ..utils.state import CreateReminderState

# декоратор dispatcher.message_handler
# используется для объявления обработчика событий
# прихода нового сообщения

# обработчик для /start команды (начало диалога с ботом)
@dispatcher.message_handler(commands=["start"])
async def start_handler(message: types.Message):
    # message.answer используется для ответа на сообщение
    await message.answer(bot.phrases.start_message, reply_markup=markups.start_markup)


# обработчик для сообщения о создании напоминания
@dispatcher.message_handler(lambda m: m.text == bot.phrases.create_reminder)
async def create_reminder_handler(message: types.Message):
    # задание состояния CreateReminderState, сначала бот ожидает дату
    # прихода напоминания
    await CreateReminderState.waiting_datetime.set()
    await message.answer(bot.phrases.enter_datetime)


# обработчик для даты напоминания
@dispatcher.message_handler(state=CreateReminderState.waiting_datetime)
async def datetime_handler(message: types.Message, state: FSMContext):
    try:
        # парсин даты
        send_at_datetime = dt.datetime.strptime(message.text, r"%d.%m.%Y %H:%M")
        now = dt.datetime.now()

    except Exception:
        # если формат даты неправильный
        # то обрабатывается исключение и присылается сообщение
        return await message.answer(bot.phrases.enter_datetime)

    # сохранение даты в состояние
    async with state.proxy() as data:
        data["send_at"] = send_at_datetime

    # задание состояния ожидания сообщения напоминания
    await CreateReminderState.waiting_message.set()
    await message.answer(bot.phrases.enter_message)


# обработчик сообщения напоминания
@dispatcher.message_handler(
    state=CreateReminderState.waiting_message, content_types=types.ContentTypes.ANY
)
async def reminder_message_handler(message: types.Message, state: FSMContext):
    # открытие данных состояния
    # и создание записи о напоминании в базе данных
    # с помощью метода модели Reminder.create
    async with state.proxy() as data:
        await Reminder.create(
            **data, message_id=message.message_id, user_id=message.from_user.id
        )

    # завершение состояния и сообщение о создании напоминания
    await state.finish()
    await message.answer(bot.phrases.reminder_created)


# для отслеживания завершенности напоминаний
# используется библиотека aioschedule

# функция работы для напоминаний
# работа задается в aioschedule в файле __main__.py
async def reminders_job():
    # получение истекших напоминаний
    now = dt.datetime.now()
    reminders_query = Reminder.filter(send_at__lte=now)
    reminders = await reminders_query.all()

    if not reminders:
        return

    # итерация по напоминаниям
    for reminder in reminders:
        try:
            # копирование сообщения напоминания
            # создателю напоминания
            await bot.copy_message(
                reminder.user_id, reminder.user_id, reminder.message_id
            )
        except Exception:
            continue

    # удаление истекших напоминаний из базы
    await reminders_query.delete()
