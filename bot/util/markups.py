from aiogram.types import KeyboardButton, ReplyKeyboardMarkup

from ..bot import bot

start_markup = ReplyKeyboardMarkup(resize_keyboard=True)
start_markup.row(
    KeyboardButton(bot.phrases.create_reminder),
)
