from aiogram.dispatcher.filters.state import State, StatesGroup


class CreateReminderState(StatesGroup):
    waiting_datetime = State()
    waiting_message = State()
