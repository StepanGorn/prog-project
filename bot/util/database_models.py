from tortoise import Model, fields


class Reminder(Model):
    id = fields.IntField(pk=True, unique=True)
    user_id = fields.IntField()
    message_id = fields.IntField()
    send_at = fields.DatetimeField()
