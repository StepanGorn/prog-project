import asyncio
import logging

import aioschedule
from aiogram import executor
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from tortoise import Tortoise

from . import root_path
from .bot import bot, dispatcher
from .handlers.start import reminders_job

handlers_path = root_path / "bot" / "handlers"

# функция для импорта обработчиков
def load_handlers():
    for filepath in handlers_path.glob("*.py"):
        __import__(f"bot.handlers.{filepath.stem}")


# функция для запуска работ в aioschedule
async def run_pending_jobs():
    TIMEOUT = 10

    while True:
        await aioschedule.run_pending()
        await asyncio.sleep(TIMEOUT)


# функция main - входная точка программы
def main():
    log_filename = str((root_path / "logs.log").resolve())

    # инициализация логов
    logging.basicConfig(
        filename=log_filename,
        level=logging.ERROR,
        format=r"%(asctime)s %(levelname)s %(funcName)s(%(lineno)d) %(message)s",
    )

    logger = logging.getLogger("bot")
    dispatcher.middleware.setup(LoggingMiddleware(logger=logger))

    # загрузка обработчиков
    load_handlers()

    # callback события при запуске бота
    async def on_startup(*_):
        # инициализация подключения и создание моделей
        # для Tortoise ORM
        await Tortoise.init(
            modules={"models": ["bot.utils.database_models"]},
            db_url=f"sqlite://{root_path / 'database.sqlite3'}",
        )

        await Tortoise.generate_schemas()

        # получение данных о пользователе бота
        me = await bot.get_me()
        print(bot.phrases.bot_started.format(bot=me))  # отправка сообщение о запуске

        # запуск задач в aioschedule
        aioschedule.every(1).minute.do(reminders_job)
        bot.loop.create_task(run_pending_jobs())

    # callback события завершения процесса программы
    async def on_shutdown(*_):
        # закрытие соединений с базой данных
        await Tortoise.close_connections()

    # запуск бота
    executor.start_polling(
        dispatcher,
        skip_updates=False,
        loop=bot.loop,
        on_startup=on_startup,
        on_shutdown=on_shutdown,
    )


# вызов функции main
main()
